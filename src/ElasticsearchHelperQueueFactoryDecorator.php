<?php

namespace Drupal\elasticsearch_helper;

use Drupal\Core\Queue\QueueFactory;

/**
 * QueueFactory decorator.
 *
 * Decorates the core QueueFactory to modify get() method
 * and return a custom Queue for Elasticsearch Helper.
 */
class ElasticsearchHelperQueueFactoryDecorator extends QueueFactory {

  /**
   * The inner queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $innerService;

  /**
   * The queue factory.
   *
   * @var \Drupal\elasticsearch_helper\ElasticsearchHelperQueueFactory
   */
  protected $queueFactory;

  /**
   * ElasticsearchHelperQueueFactoryDecorator constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $inner
   * @param \Drupal\elasticsearch_helper\ElasticsearchHelperQueueFactory $queue_factory
   */
  public function __construct(QueueFactory $inner, ElasticsearchHelperQueueFactory $queue_factory) {
    $this->innerService = $inner;
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function get($name, $reliable = FALSE) {
    // Use module's own queue implementation.
    if ($name == 'elasticsearch_helper_indexing') {
      if (!isset($this->queues[$name])) {
        $queue = $this->queueFactory->get($name);
        $this->queues[$name] = $queue;
      }

      return $this->queues[$name];
    }

    // Use core service for everything else.
    return $this->innerService->get($name, $reliable);
  }

}
